import json
import multiprocessing
import os

workers_per_core_str = os.getenv("GUN_WORKERS_PER_CORE", "2")
max_workers_str = os.getenv("GUN_MAX_WORKERS")
use_max_workers = None
if max_workers_str:
    use_max_workers = int(max_workers_str)
web_concurrency_str = os.getenv("GUN_WEB_CONCURRENCY", None)

host = os.getenv("HOST", "0.0.0.0")
port = os.getenv("PORT", "9008")
bind_env_sock = os.getenv("GUN_BIND_SOCK", None)
bind_env_ip = os.getenv("GUN_BIND_IP", None)

use_loglevel = os.getenv("GUN_LOG_LEVEL", "info")
use_bind = []
if bind_env_sock:
    use_bind.append(bind_env_sock)
if bind_env_ip:
    use_bind.append(bind_env_ip)
if not len(use_bind):
    use_bind.append(f"{host}:{port}")


cores = multiprocessing.cpu_count()
workers_per_core = float(workers_per_core_str)
default_web_concurrency = workers_per_core * cores
if web_concurrency_str:
    web_concurrency = int(web_concurrency_str)
    assert web_concurrency > 0
else:
    web_concurrency = max(int(default_web_concurrency), 2)
    if use_max_workers:
        web_concurrency = min(web_concurrency, use_max_workers)
accesslog_var = os.getenv("GUN_ACCESS_LOG", "-")
use_accesslog = accesslog_var or None
errorlog_var = os.getenv("GUN_ERROR_LOG", "-")
use_errorlog = errorlog_var or None
graceful_timeout_str = os.getenv("GUN_GRACEFUL_TIMEOUT", "120")
timeout_str = os.getenv("GUN_TIMEOUT", "120")
keepalive_str = os.getenv("GUN_KEEP_ALIVE", "5")

# Gunicorn config variables
loglevel = use_loglevel
workers = web_concurrency
bind = use_bind
errorlog = use_errorlog
worker_tmp_dir = "/dev/shm"
worker_class = "uvicorn.workers.UvicornWorker"
accesslog = use_accesslog
graceful_timeout = int(graceful_timeout_str)
timeout = int(timeout_str)
keepalive = int(keepalive_str)


if __name__ == "__main__":
    # For debugging and testing
    log_data = {
        "loglevel": loglevel,
        "workers": workers,
        "bind": bind,
        "graceful_timeout": graceful_timeout,
        "timeout": timeout,
        "keepalive": keepalive,
        "errorlog": errorlog,
        "accesslog": accesslog,
        # Additional, non-gunicorn variables
        "workers_per_core": workers_per_core,
        "use_max_workers": use_max_workers,
        "host": host,
        "port": port,
    }
    print(json.dumps(log_data))
