from fastapi import FastAPI
import os
app = FastAPI()


GUN_ENV_VARS = {k: v for k, v in os.environ.items() if str(k).startswith('GUN_')}


@app.get("/api/v1/")
async def read_root():
    message = "The app is running by Gunicorn. Now your solution is ready. Deploy your app and restart Gunicorn"
    return {"message": message}


@app.get("/api/v1/ping")
async def send_pong():
    message = "pong"
    return {"message": message}


@app.get("/api/v1/gun_vars")
async def send_gun_vars():
    return GUN_ENV_VARS
